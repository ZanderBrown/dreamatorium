use std::iter::Peekable;
use std::str::Chars;
use std::fmt;

#[derive(Debug)]
pub struct Syntax(String, usize, usize);

impl fmt::Display for Syntax {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Syntax Error: {} [{}:{}]", self.0, self.1, self.2)
    }
}

/// Wraps a string in an iter-like structure
/// that tracks the current line-number & column
pub struct Input<'a> {
    input: Peekable<Chars<'a>>,
    line: usize,
    col: usize,
}

impl<'a> Input<'a> {
    /// Create a stream from a string
    pub fn new(input: Chars<'a>) -> Self {
        Self {
            input: input.peekable(),
            line: 1,
            col: 0,
        }
    }

    /// Generate a syntax error with msg at the current position
    pub fn croak(&self, msg: String) -> Syntax {
        Syntax(msg, self.line, self.col)
    }

    /// Consume a char and advance position
    pub fn forward(&mut self) {
        // If further chars are avalible
        if let Some(ch) = self.input.next() {
            // If the charecter is a newline
            if ch == '\n' {
                // Record the new line position
                self.line += 1;
                // Reset the column position
                self.col = 0;
            } else {
                // Still on the same line, advance the column
                self.col += 1;
            }
        }
    }

    pub fn peek(&mut self) -> Option<char> {
        self.input.peek().map(|ch| *ch)
    }

    pub fn read(&mut self, matcher: &Fn(char) -> bool) -> String {
        // The string we will return
        let mut s = String::new();
        // Read whilst charecters are still available
        while let Some(ch) = self.peek() {
            // If ch satisfies predicate
            if matcher(ch) {
                // Consume ch appending it to the result
                s.push(ch);
                self.forward();
            } else {
                // Break out of the while loop
                break;
            }
        }
        s
    }
}

/// Allow casting String to Input
impl<'a> From<Chars<'a>> for Input<'a> {
    fn from(s: Chars<'a>) -> Self {
        Self::new(s)
    }
}
