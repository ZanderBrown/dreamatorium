use input::Syntax;
use token::{Token, Tokens};

pub struct ObjectDecl {
    name: String,
    is: Option<String>,
    does: Vec<String>,
    decls: Vec<VarDecl>,
}

pub struct VarDecl {
    name: String,
    form: String,
}

pub enum Node {
    ObjectDecl(ObjectDecl),
    VarDecl(VarDecl),
}

trait Parser {
    fn require(&mut self, tok: &Token) -> Result<(), Syntax>;
    fn possibly(&mut self, tok: &Token) -> Result<bool, Syntax>;
    fn require_symbol(&mut self, sym: Option<&str>) -> Result<String, Syntax>;
    fn possibly_symbol(&mut self, sym: &str) -> Result<bool, Syntax>;

    fn parse_object(&mut self) -> Result<Node, Syntax>;
    fn parse_var(&mut self) -> Result<VarDecl, Syntax>;
}

fn delimited<'a, T>(
    input: &mut Tokens,
    stop: &'a [Token],
    separator: &Token,
    parser: &mut FnMut(&mut Tokens) -> Result<T, Syntax>,
) -> Result<Vec<T>, Syntax> {
    // The list we will return
    let mut elements: Vec<T> = Vec::new();
    // Are we still on the first element
    let mut first = true;
    'read: loop {
        // If there is more tokens available
        if input.peek()?.is_some() {
            // We have to support multiple close tokens for things like
            // if statments where we could be looking for ELSE or ENDIF
            for stop in stop {
                // If we matched a stop token
                if input.possibly(stop)? {
                    // End the read loop
                    break 'read;
                }
            }
            // If we are still at the first element
            if first {
                // Clear the flag
                first = false;
            } else {
                // Every element past the first should be preceeded
                // by the seperator token so move over it
                input.require(separator)?;
            }
            // Same as above
            for stop in stop {
                if input.possibly(stop)? {
                    break 'read;
                }
            }
            // Call the provided parser appending the result
            elements.push(parser(input)?);
        } else {
            // Run out of input
            return Err(input.croak("End of file whilst in list".into()));
        }
    }
    // Return the elements
    Ok(elements)
}

impl<'a> Parser for Tokens<'a> {
    fn require(&mut self, tok: &Token) -> Result<(), Syntax> {
        if let Some(t) = self.next()? {
            if t == *tok {
                Ok(())
            } else {
                Err(self.croak(format!("Expected {}, found {}", tok, t)))
            }
        } else {
            Err(self.croak("Expected symbol, found end of file".into()))
        }
    }

    fn possibly(&mut self, tok: &Token) -> Result<bool, Syntax> {
        if let Some(s) = self.peek()? {
            if *tok == s {
                return Ok(true);
            }
        }
        Ok(false)
    }

    fn require_symbol(&mut self, sym: Option<&str>) -> Result<String, Syntax> {
        if let Some(t) = self.next()? {
            if let Token::Symbol(val) = t.clone() {
                if let Some(sym) = sym {
                    if sym == val {
                        return Ok(val);
                    }
                } else {
                    return Ok(val);
                }
            }
            if let Some(sym) = sym {
                return Err(self.croak(format!("Expected `{}', found {}", sym, t)));
            }
        }
        Err(self.croak("Expected symbol, found end of file".into()))
    }

    fn possibly_symbol(&mut self, sym: &str) -> Result<bool, Syntax> {
        if let Some(s) = self.peek()? {
            if let Token::Symbol(val) = s {
                if val == sym {
                    return Ok(true);
                }
            }
        }
        Ok(false)
    }

    fn parse_object(&mut self) -> Result<Node, Syntax> {
        let name = self.require_symbol(None)?;
        let is = {
            if self.possibly_symbol("is")? {
                self.require_symbol(Some("is"))?;
                None
            } else {
                None
            }
        };
        let does = {
            if self.possibly_symbol("does")? {
                self.require_symbol(Some("does"))?;
                delimited(
                    self,
                    &[Token::Colon, Token::LBrace],
                    &Token::Comma,
                    &mut |i| i.require_symbol(None),
                )?
            } else {
                vec![]
            }
        };
        let decls = {
            if self.possibly(&Token::Colon)? {
                self.require(&Token::Colon)?;
                Vec::with_capacity(0)
            } else {
                self.require(&Token::LBrace)?;
                let res = delimited(self, &[Token::RBrace], &Token::Comma, &mut |i| {
                    i.parse_var()
                })?;
                self.require(&Token::RBrace)?;
                res
            }
        };
        Ok(Node::ObjectDecl(ObjectDecl {
            name,
            is,
            does,
            decls,
        }))
    }

    fn parse_var(&mut self) -> Result<VarDecl, Syntax> {
        let name = self.require_symbol(None)?;
        self.require_symbol(Some("is"))?;
        let form = self.require_symbol(None)?;
        Ok(VarDecl { name, form })
    }
}

pub fn parse(input: &mut Tokens) -> Result<Vec<Node>, Syntax> {
    let mut nodes = Vec::with_capacity(100); // Seems a resonable start
    while let Some(tok) = input.next()? {
        nodes.push(match tok {
            Token::Symbol(sym) => match sym.as_ref() {
                "object" => input.parse_object()?,
                sym => {
                    println!("{}", sym);
                    Node::VarDecl(VarDecl {
                        name: "test".into(),
                        form: "thing".into(),
                    })
                }
            },
            tok => {
                print!("{}", tok);
                Node::VarDecl(VarDecl {
                    name: "test".into(),
                    form: "thing".into(),
                })
            }
        })
    }
    Ok(nodes)
}
