use std::fmt;

use input::{Input, Syntax};

#[derive(Clone, PartialEq)]
pub enum Token {
    Symbol(String),
    Operator(String, u8),
    Text(String),
    Num(String),
    LBrace,
    RBrace,
    Colon,
    LSquare,
    RSquare,
    LBrack,
    RBrack,
    Comma,
}

impl fmt::Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Token::Symbol(s) => s.clone(),
                Token::Operator(ch, _) => ch.clone(),
                Token::Text(t) => format!("\"{}\"", t),
                Token::Num(n) => n.clone(),
                Token::LBrace => "{".into(),
                Token::RBrace => "}".into(),
                Token::Colon => ";".into(),
                Token::LSquare => "[".into(),
                Token::RSquare => "]".into(),
                Token::LBrack => "(".into(),
                Token::RBrack => ")".into(),
                Token::Comma => ",".into(),
            }
        )
    }
}

pub fn is_op (s: &str) -> Option<u8> {
    match s {
        "=" => Some(1),
        "or" => Some(2),
        "and" => Some(3),
        "<" | ">" | "<=" | ">=" | "==" | "!=" => Some(7),
        "+" | "-" => Some(10),
        "*" | "/" | "%" => Some(20),
        _ => None
    }
}

pub struct Tokens<'a> {
    // Caches a token produced for peek()
    current: Option<Token>,
    input: Input<'a>,
}

impl<'a> Tokens<'a> {
    pub fn new (input: Input<'a>) -> Self {
        Self {
            current: None,
            input
        }
    }
    fn read_next(&mut self) -> Result<Option<Token>, Syntax> {
        self.input.read(&|ch| ch.is_whitespace());
        match self.input.peek() {
            Some('/') => {
                self.input.forward();
                match self.input.peek() {
                    Some('/') => {
                        self.input.forward();
                        self.input.read(&|ch| ch != '\n');
                        self.read_next()
                    },
                    Some (_) | None => Ok(Some(Token::Operator("/".to_string(), 20))),
                }
            }
            Some('"') => Ok(Some(Token::Text({
                self.input.forward();
                let res = self.input.read(&|ch| ch != '"');
                self.input.forward();
                res
            }))),
            Some(ch) if ch.is_numeric() => Ok(Some(Token::Num(self.input.read(&|ch| ch.is_numeric())))),
            Some(ch) if ch.is_alphabetic() => {
                Ok(Some({
                    let res = self.input.read(&|ch| !ch.is_whitespace());
                    if let Some(prec) = is_op(&res) {
                        Token::Operator(res, prec)
                    } else {
                        Token::Symbol(res)
                    }
                }))
            }
            Some(ch) => {
                self.input.forward();
                match ch {
                    '{' => Ok(Some(Token::LBrace)),
                    '}' => Ok(Some(Token::RBrace)),
                    ';' => Ok(Some(Token::Colon)),
                    '[' => Ok(Some(Token::LSquare)),
                    ']' => Ok(Some(Token::RSquare)),
                    '(' => Ok(Some(Token::LBrack)),
                    ')' => Ok(Some(Token::RBrack)),
                    ',' => Ok(Some(Token::Comma)),
                    _ => {
                        if let Some(_) = is_op(&ch.to_string()) {
                            let res = self.input.read(&|ch| is_op(&ch.to_string()).is_some());
                            if let Some(prec) = is_op(&res) {
                                Ok(Some(Token::Operator(res, prec)))
                            } else {
                                Err(self.input.croak(format!("Unknown '{}'", ch)))
                            }
                        } else {
                            Err(self.input.croak(format!("Unknown '{}'", ch)))
                        }
                    },
                }
            }
            None => Ok(None),
        }
    }

    /// Fetch a token without consuming it
    pub fn peek(&mut self) -> Result<Option<Token>, Syntax> {
        // If we don't have a cached token
        if self.current.is_none() {
            // Get one
            self.current = self.read_next()?;
        }
        // Return the cached token
        Ok(self.current.clone())
    }

    /// Fetch & consume a token
    pub fn next(&mut self) -> Result<Option<Token>, Syntax> {
        // If there is a cached token
        if let Some(tok) = self.current.take() {
            // Clear it & return it
            Ok(Some(tok))
        // If we can read another token
        } else if let Some(ret) = self.read_next()? {
            // Return it
            Ok(Some(ret))
        } else {
            // We are out of tokens
            Ok(None)
        }
    }

    /// Wrapper of Input::croak
    pub fn croak(&self, msg: String) -> Syntax {
        self.input.croak(msg)
    }
}
